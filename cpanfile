requires 'Apache::Htgroup', '1.23';
requires 'Apache::Htpasswd', '1.8';
requires 'Array::Utils', '0.5';
requires 'autovivification', '0.12';
requires 'Data::UUID', '1.219';
requires 'DBI', '1.63';
requires 'File::Monitor', '1.00';
requires 'HTTP::Async', '0.23';
requires 'HTTP::Request', '6.00';
requires 'IO::All', '0.54';
requires 'JSON', '2.61';
requires 'LWP::UserAgent', '6.05';
requires 'Net::DNS', '0.68';
requires 'Net::DNS::Async', '1.07';
requires 'Regexp::Wildcards', '1.05';
requires 'String::CRC32', '1.5';
requires 'String::Interpolate', '0.32';
requires 'Text::LevenshteinXS', '0.03';
requires 'Tie::CPHash', '1.06';
requires 'URI::Escape', '3.31';

requires 'DBI', '1.63';
requires 'DBD::mysql', '4.025';

# vim: ft=perl
